const swaggerJSDoc = require('swagger-jsdoc');

const swaggerHost = process.env.SWAGGER_HOST || 'http://192.168.5.96:3002';

const swaggerDefinition = {
  info: {
    title: 'Planetfiber API Definition.',
    version: '1.0.0',
    description: 'Planetfiber Backend API',
    contact: {
      email: 'davit.zohrabyan@caiman.am',
    },
  },
  host: swaggerHost.replace('http://', '').replace('https://', ''),
  basePath: '/',
  schemes: 'http',
  consumes: ['application/x-www-form-urlencoded', 'application/json'],
  produces: 'application/json',
};

const options = {
  swaggerDefinition,
  apis: ['./swagger/**/*.yaml'],
};

module.exports = function SwaggerConfig() {
  const swaggerDocument = swaggerJSDoc(options);
  return swaggerDocument;
};
