module.exports = (sequelize, DataTypes) => {
  const contact = sequelize.define('contact', {
    name_en: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    name_ru: DataTypes.STRING,
    name_hy: DataTypes.STRING,
    phone: DataTypes.STRING,
    email: DataTypes.STRING,
    address_en: DataTypes.STRING,
    address_ru: DataTypes.STRING,
    address_hy: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    tableName: 'contacts',
    underscored: true,
    paranoid: true,
    defaultScope: {
      attributes: { exclude: ['deletedAt'] },
    },
    hooks: {
      afterSave: async (n) => {
        // eslint-disable-next-line no-param-reassign
        if (n && n.dataValues) ['deletedAt'].forEach((prop) => delete n.dataValues[prop]);
      },
    },
  });
  contact.associate = function associate(models) {
    models.contact.belongsTo(models.user, { foreignKey: 'createdBy' });
    models.contact.belongsTo(models.user, { foreignKey: 'updatedBy' });
  };
  return contact;
};
