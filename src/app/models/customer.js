module.exports = (sequelize, DataTypes) => {
  const customer = sequelize.define('customer', {
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    tableName: 'customers',
    underscored: true,
    paranoid: true,
    defaultScope: {
      attributes: { exclude: ['deletedAt'] },
    },
    hooks: {
      afterSave: async (n) => {
        // eslint-disable-next-line no-param-reassign
        if (n && n.dataValues) ['deletedAt'].forEach((prop) => delete n.dataValues[prop]);
      },
    },
  });

  customer.associate = function associate(models) {
    models.customer.belongsTo(models.user, { foreignKey: 'createdBy' });
    models.customer.belongsTo(models.user, { foreignKey: 'updatedBy' });
  };
  return customer;
};
