import { ARRAY_TYPES } from '../../data/TYPES';

module.exports = (sequelize, DataTypes) => {
  const file = sequelize.define('file', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      unique: true,
    },
    name: DataTypes.STRING,
    path: DataTypes.STRING,
    extension: DataTypes.STRING,
    size: {
      type: DataTypes.DECIMAL(15, 2),
      get() {
        const rawValue = this.getDataValue('size');
        return rawValue && Number(rawValue);
      }
    },
    type: {
      type: DataTypes.ENUM(ARRAY_TYPES.fileType),
      defaultValue: 'FILE',
    },
    dimensions: DataTypes.JSON,
    isMain: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    tableName: 'files',
    underscored: true,
    paranoid: true,
    defaultScope: {
      attributes: { exclude: ['deletedAt'] },
    },
    hooks: {
      afterSave: async (n) => {
        // eslint-disable-next-line no-param-reassign
        if (n && n.dataValues) ['deletedAt'].forEach((prop) => delete n.dataValues[prop]);
      },
    },
  });
  file.associate = function associate(models) {
    models.file.belongsTo(models.file, { as: 'parent' });
    models.file.belongsTo(models.product);
  };
  return file;
};
