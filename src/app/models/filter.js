module.exports = (sequelize, DataTypes) => {
  const filter = sequelize.define('filter', {
    title_en: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    title_ru: DataTypes.STRING,
    title_hy: DataTypes.STRING,
    subtitle_en: DataTypes.STRING,
    subtitle_ru: DataTypes.STRING,
    subtitle_hy: DataTypes.STRING,
    isHidden: DataTypes.BOOLEAN,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    tableName: 'filters',
    underscored: true,
    paranoid: true,
    defaultScope: {
      attributes: { exclude: ['deletedAt'] },
    },
    hooks: {
      afterSave: async (n) => {
        // eslint-disable-next-line no-param-reassign
        if (n && n.dataValues) ['deletedAt'].forEach((prop) => delete n.dataValues[prop]);
      },
    },
  });
  filter.associate = function associate(models) {
    models.filter.belongsTo(models.user, { foreignKey: 'createdBy' });
    models.filter.belongsTo(models.user, { foreignKey: 'updatedBy' });
  };
  return filter;
};
