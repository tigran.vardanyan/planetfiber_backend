/* eslint-disable no-console */
import fs from 'fs';
import path from 'path';
import Sequelize from 'sequelize';
import { exec } from 'child_process';

const db = {};
const force = process.env.DB_FORCE === 'true';

export function setUpDb(sequelize) {
  const basename = path.basename(__filename);

  const getDirectories = (source) => fs.readdirSync(source, { withFileTypes: true })
    .filter((dirent) => dirent.isDirectory())
    .map((dirent) => dirent.name);

  const createPrefix = (prefix, dir) => {
    // eslint-disable-next-line no-param-reassign
    prefix = prefix !== '' ? `${prefix}_` : prefix;
    fs.readdirSync(dir)
      .filter((file) => (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js')).forEach((file) => {
        const model = sequelize.import(path.join(dir, file));
        model.tableName = prefix + model.tableName;
        db[model.name] = model;
      });
    getDirectories(dir).forEach((child) => createPrefix(prefix + child, path.join(dir, child)));
  };
  createPrefix('', __dirname);
  Object.keys(db).forEach((modelName) => {
    if (db[modelName].associate) {
      db[modelName].associate(db);
    }
  });
  db.sequelize = sequelize;
  db.Sequelize = Sequelize;
  db.Op = Sequelize.Op;
  sequelize.sync({ force })
    .then(() => {
      // eslint-disable-next-line no-unused-expressions
      process.env.NODE_ENV !== 'production' && force && exec('npm run seed', (error) => error && console.error(error)).stdout.pipe(process.stdout);
    });
  return db;
}

export default db;
