module.exports = (sequelize, DataTypes) => {
  const attribute = sequelize.define('attribute', {
    description_en: DataTypes.STRING,
    description_ru: DataTypes.STRING,
    description_hy: DataTypes.STRING,
    unit: DataTypes.STRING,
    type: DataTypes.ENUM('enum', 'number'),
    values: DataTypes.JSON,
    step: DataTypes.INTEGER,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    tableName: 'attributes',
    underscored: true,
    paranoid: true,
    defaultScope: {
      attributes: { exclude: ['deletedAt'] },
    },
    hooks: {
      afterSave: async (n) => {
        // eslint-disable-next-line no-param-reassign
        if (n && n.dataValues) ['deletedAt'].forEach((prop) => delete n.dataValues[prop]);
      },
    },
  });
  attribute.associate = function associate(models) {
    models.attribute.belongsToMany(models.category, { through: 'category_attributes' });
    models.attribute.belongsToMany(models.product, { through: models.product_attribute });
    models.attribute.belongsTo(models.user, { foreignKey: 'createdBy' });
    models.attribute.belongsTo(models.user, { foreignKey: 'updatedBy' });
  };
  return attribute;
};
