module.exports = (sequelize, DataTypes) => {
  const section = sequelize.define('section', {
    title_en: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    title_ru: DataTypes.STRING,
    title_hy: DataTypes.STRING,
    isHidden: DataTypes.BOOLEAN,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    tableName: 'sections',
    underscored: true,
    paranoid: true,
    defaultScope: {
      attributes: { exclude: ['deletedAt'] },
    },
    hooks: {
      afterSave: async (n) => {
        // eslint-disable-next-line no-param-reassign
        if (n && n.dataValues) ['deletedAt'].forEach((prop) => delete n.dataValues[prop]);
      },
    },
  });
  section.associate = function associate(models) {
    models.section.belongsToMany(models.product, { through: 'product_sections' });
    models.section.belongsTo(models.user, { foreignKey: 'createdBy' });
    models.section.belongsTo(models.user, { foreignKey: 'updatedBy' });
  };
  return section;
};
