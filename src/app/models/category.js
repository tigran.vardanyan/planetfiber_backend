module.exports = (sequelize, DataTypes) => {
  const category = sequelize.define('category', {
    key: {
      type: DataTypes.STRING,
      unique: true,
      // allowNull: false
    },
    title_en: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    title_ru: DataTypes.STRING,
    title_hy: DataTypes.STRING,
    subtitle_en: DataTypes.STRING,
    subtitle_ru: DataTypes.STRING,
    subtitle_hy: DataTypes.STRING,
    position: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    isHidden: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    tableName: 'categories',
    underscored: true,
    paranoid: true,
    defaultScope: {
      attributes: { exclude: ['deletedAt'] },
    },
    hooks: {
      afterSave: async (n) => {
        // eslint-disable-next-line no-param-reassign
        if (n && n.dataValues) ['deletedAt'].forEach((prop) => delete n.dataValues[prop]);
      },
    },
  });
  category.associate = function associate(models) {
    models.category.belongsTo(models.category, { as: 'parent' });
    models.category.belongsToMany(models.attribute, { through: 'category_attributes' });
    models.category.belongsToMany(models.product, { through: 'product_categories' });
    models.category.belongsTo(models.user, { foreignKey: 'createdBy' });
    models.category.belongsTo(models.user, { foreignKey: 'updatedBy' });
  };
  return category;
};
