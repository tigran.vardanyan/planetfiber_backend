import * as controller from './controller';
import validator from './validator';
import { onCreateUserData, onUpdateUserData } from '../../utils/middlers';

export default (router) => {
  // ----------------------------------------------------------------------------------------------
  router.get('/', validator.getAll, controller.getAll);
  // ----------------------------------------------------------------------------------------------
  router.get('/:id', validator.isExist, controller.get);
  // ----------------------------------------------------------------------------------------------
  router.post('/', validator.store, onCreateUserData, controller.store);
  // ----------------------------------------------------------------------------------------------
  router.patch('/:id', validator.update, onUpdateUserData, controller.update);
  // ----------------------------------------------------------------------------------------------
  router.delete('/:id', validator.isExist, onUpdateUserData, controller.remove);
  // ----------------------------------------------------------------------------------------------
  router.patch('/revert/:id', validator.isDeleted, onUpdateUserData, controller.revert);
};
