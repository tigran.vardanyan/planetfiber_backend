import { checkSchema } from 'express-validator/check';
import { coreValidator, errorMessage } from '../../core/validator';
import repository from './repository';

const validator = Object.create(coreValidator);
const validationFields = {

  include: {
    ...coreValidator.queryString,
    custom: {
      errorMessage: errorMessage.includeExclude,
      options: (include, { req }) => {
        if (req.query.exclude) return false;
        return true;
      },
    },
  },
  exclude: {
    ...coreValidator.queryString,
    custom: {
      errorMessage: errorMessage.includeExclude,
      options: (exclude, { req }) => {
        if (req.query.include) return false;
        return true;
      },
    },
  },
};
export default Object.assign(validator, {
  store: checkSchema({}),

  update: checkSchema({
    id: coreValidator.modelIdInParams(repository.modelName),
  }),

  isExist: checkSchema({
    id: coreValidator.modelIdInParams(repository.modelName),
  }),

  getAll: checkSchema({
    offset: { ...coreValidator.offset, optional: true },
    limit: { ...coreValidator.limit, optional: true },
    include: { ...validationFields.include, optional: true },
    exclude: { ...validationFields.exclude, optional: true },
  }),

  isDeleted: checkSchema({
    id: coreValidator.isNotDeletedResource(repository.modelName),
  }),
});
