import coreService from '../../core/service';
import repository from './repository';
import models from '../../models';
import { ServiceError } from '../../utils/error_handler';

const service = Object.create(coreService);

export default Object.assign(service, {
  /**
   *  Get All available resources
   *  @return {Array} array of resource ORM objects
   */
  getAll(query) {
    try {
      return repository.findAll(
        { name: { [models.Op.startsWith]: query.name || '' } },
        {
          attributes: ['id', 'name', ['code2l', 'code']],
          offset: Number(query.offset) || 0,
          limit: Number(query.limit) || 20,
        },
      );
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Get One available resources
   *  @param {Number} pk : resource ID
   *  @return {Object} resource ORM object
   */
  getByPk(pk) {
    try {
      return repository.findByPk(pk, { attributes: ['id', 'name', 'code2l', 'code3l', 'nameOfficial', 'latitude', 'longitude', 'zoom', 'phoneCode'] });
    } catch (error) {
      throw new ServiceError(error);
    }
  },
});
