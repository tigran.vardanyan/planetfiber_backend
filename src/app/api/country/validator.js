import { checkSchema } from 'express-validator/check';
import { coreValidator } from '../../core/validator';
import repository from './repository';

const validator = Object.create(coreValidator);

export default Object.assign(validator, {
  isExist: checkSchema({
    id: coreValidator.modelIdInParams(repository.modelName),
  }),

  getAll: checkSchema({
    name: { ...coreValidator.stringDefaultInQuery, optional: true },
    offset: { ...coreValidator.offset, optional: true },
    limit: { ...coreValidator.limit, optional: true },
  }),
});
