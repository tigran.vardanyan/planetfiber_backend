import coreRepository from '../../core/repository';
import models from '../../models';
import { RepositoryError } from '../../utils/error_handler';

const repository = Object.create(coreRepository);

export default Object.assign(repository, {
  modelName: 'user',

  async findUser(data) {
    try {
      const where = { $or: [] };
      if (data.username) {
        where.$or.push({ username: { $eq: data.username } });
      } else if (data.email) {
        where.$or.push({ email: { $eq: data.email } });
      }

      const response = await models.user.findOne({
        where,
        attributes: { include: ['id', 'username', 'email', 'password'] },
      });
      return response;
    } catch (error) {
      throw new RepositoryError(error);
    }
  },
});
