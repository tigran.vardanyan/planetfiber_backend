import coreService from '../../core/service';
import { ServiceError } from '../../utils/error_handler';

const util = require('util');
const exec = util.promisify(require('child_process').exec);

const service = Object.create(coreService);

export default Object.assign(service, {

  async migrateRun() {
    try {
      // return exec('sequelize db:migrate');
      // getPolygonOsmAddress();
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  migrateUndo(query) {
    try {
      const { fileName } = query;

      return exec(`sequelize db:migrate:undo --name ${fileName}`);
    } catch (error) {
      throw new ServiceError(error);
    }
  },

});
