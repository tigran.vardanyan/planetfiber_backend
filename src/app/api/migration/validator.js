import { checkSchema } from 'express-validator/check';
import { coreValidator, errorMessage } from '../../core/validator';

const fs = require('fs');
const Path = require('path');

const validator = Object.create(coreValidator);

const validationFields = {
  fileNameInQuery: {
    in: ['query'],
    errorMessage: errorMessage.fieldRequired,
    isEmpty: { negated: true },
    isString: true,
    custom: {
      errorMessage: (fileName) => errorMessage.isFileExists(fileName),
      options(fileName) {
        const path = Path.join(__dirname, '../../../../migrations/', fileName);

        const isExist = fs.existsSync(path);
        if (!isExist) {
          return false;
        }
        return true;
      },
    },
  },

};

export default Object.assign(validator, {
  migrateRun: checkSchema({
    // fileName: validationFields.fileNameInQuery,
  }),

  migrateUndo: checkSchema({
    fileName: validationFields.fileNameInQuery,
  }),

});
