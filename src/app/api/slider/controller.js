import service from './service';
import cw from '../../core/controller';

// eslint-disable-next-line import/prefer-default-export
export const getAll = cw((req) => service.getAll(req.language));
