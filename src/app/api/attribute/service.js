import coreService from '../../core/service';
import repository from './repository';
import models from '../../models';
import { ServiceError } from '../../utils/error_handler';
import { operatorsAliases as Op } from '../../../../sequelizeConfig';

const service = Object.create(coreService);

export default Object.assign(service, {
  /**
   *  Get All available resources
   *  @return {Array} array of resource ORM objects
   */
  async getAll(query, language) {
    try {
      const where = {};
      if (query.search)
        where[`description_${language}`] = { [Op.$iLike]: `%${query.search}%` };
      const categoriesQuery = {};
      if (query.categoryIds) {
        categoriesQuery.id = { $in: query.categoryIds.split(',') };
      }
      const options = {
        order: [
          ['id', 'ASC'],
          [`description_${language}`, 'ASC'],
        ],
        attributes: [
          'id',
          [`description_${language}`, 'description'],
          'unit',
          'type',
          'values',
        ],
        include: [
          {
            model: models.category,
            where: categoriesQuery,
            attributes: ['id', [`title_${language}`, 'title']],
          },
        ],
      };

      return repository.findAll(where, options);
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Get One available resources
   *  @param {Number} pk : resource ID
   *  @return {Object} resource ORM object
   */
  getByPk(pk, language) {
    try {
      const options = {
        order: [
          ['id', 'ASC'],
          [`description_${language}`, 'ASC'],
        ],
        attributes: [
          'id',
          [`description_${language}`, 'description'],
          'unit',
          'type',
          'values',
        ],
      };
      return repository.findByPk(pk, options);
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Create resources
   *  @param {Object} data : resource data object
   *  @return {Object} resource ORM object
   */
  // async store(data) {
  //   try {
  //     const created = await repository.create({
  //       ...data,
  //     });
  //     created.setDataValue('country', await created.getCountry());
  //     created.setDataValue('img', await created.getImg());
  //     return created;
  //   } catch (error) {
  //     throw new ServiceError(error);
  //   }
  // },

  /**
   *  Update resources
   *  @param {Object} data : resource data object
   *  @param {Object} id : resource ID
   *  @return {Object} resource ORM object
   */
  // async update(data, id) {
  //   try {
  //     await repository.update(
  //       data,
  //       { id },
  //     );
  //     return repository.findByPk(id, { include: ['country', 'img'] });
  //   } catch (error) {
  //     throw new ServiceError(error);
  //   }
  // },

  /**
   *  Remove resources
   *  @param {Object} id : resource ID
   *  @return {Object} resource ORM object
   */
  // remove(data, id) {
  //   try {
  //     return repository.softDeleteWithUserId(data, { id });
  //   } catch (error) {
  //     throw new ServiceError();
  //   }
  // },

  /**
   *  Revert resources
   *  @param {Object} id : resource ID
   *  @return {Object} resource ORM object
   */
  // async revert(data, id) {
  //   try {
  //     await repository.revertWithUserId(data, { id });
  //     return repository.findByPk(id, { include: ['country', 'img'] });
  //   } catch (error) {
  //     throw new ServiceError();
  //   }
  // },
});
