import fs from 'fs';
import nodemailer from 'nodemailer';
import handlebars from 'handlebars';
import path from 'path';

import coreService from '../../core/service';
import repository from './repository';
import { ServiceError } from '../../utils/error_handler';

const service = Object.create(coreService);

const mailer = nodemailer.createTransport({
  host: process.env.EMAIL_WEBSITE_HOST,
  port: '465',
  secure: '465',
  auth: {
    user: process.env.EMAIL_AUTH_USERNAME,
    pass: process.env.EMAIL_AUTH_PASSWORD,
  },
});

export default Object.assign(service, {
  /**
   *  Get All available resources
   *  @return {Array} array of resource ORM objects
   */
  async get(language) {
    try {
      const options = {
        attributes: [
          'id',
          [`name_${language}`, 'name'],
          'email',
          'phone',
          [`address_${language}`, 'address'],
        ],
      };
      const [contact] = await repository.findAll({}, options);
      return contact;
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Create resources
   *  @param {Object} data : resource data object
   *  @return {Object} resource ORM object
   */
  async create(data) {
    try {
      const { name, email, message } = data;
      const feedbackTopicTitle = 'Contact message';

      // try {
      const htmlPath = path.join(
        __dirname,
        '../../../..',
        'public',
        'email_form.html'
      );
      const html = await fs.readFileSync(htmlPath, {
        encoding: 'utf-8',
      });

      const template = handlebars.compile(html);
      const replacements = {
        name,
        email,
        feedbackTopic: feedbackTopicTitle,
        message,
      };
      const htmlToSend = template(replacements);

      const mailOptions = {
        from: process.env.EMAIL_AUTH_USERNAME,
        to: process.env.EMAIL_TO,
        replyTo: `${name} ${email}`,
        subject: 'PLANETFIBER CONTACT',
        html: htmlToSend,
      };

      try {
        await mailer.sendMail(mailOptions);
        return 'Email sent successfully';
      } catch (err) {
        throw new ServiceError(
          `The emailSender encountered an internal error: ${err.message}`
        );
      }
    } catch (error) {
      throw new ServiceError(error);
    }
  },
});
