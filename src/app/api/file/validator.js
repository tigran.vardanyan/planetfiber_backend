import { checkSchema } from 'express-validator/check';
import { coreValidator } from '../../core/validator';
import repository from './repository';

const validator = Object.create(coreValidator);

export default Object.assign(validator, {
  store: checkSchema({
    parentId: { ...coreValidator.folderInBody, optional: true },
  }),

  update: checkSchema({
    id: coreValidator.modelIdInParams(repository.modelName),
    parentId: { ...coreValidator.folderInBody, optional: true },
  }),

  isExist: checkSchema({
    id: coreValidator.modelIdInParams(repository.modelName),
  }),

  getAll: checkSchema({}),

  storeFolder: checkSchema({
    name: coreValidator.name,
    parentId: { ...coreValidator.folderInBody, optional: true },
  }),

  updateFolder: checkSchema({
    id: coreValidator.modelIdInParams(repository.modelName),
    parentId: { ...coreValidator.folderInBody, optional: true },
  }),
});
