import crypto from 'crypto';
import url from 'url';
import * as redis from './redis';
import { AuthError } from './error_handler';
import { encrypt } from './crypto_util';

const secret = process.env.APP_SECRET;

export default async function authentication(req, _res, next) {
  try {
    const token = req.headers['x-access-token'];
    const session = req.headers['x-app-session'];
    const timestamp = req.headers['x-app-timestamp'];
    const signature = req.headers['x-app-signature'];
    if (
      // req.headers.referer === 'https://dev.caiman.am/api-docs' ||
      req.headers.referer === 'http://localhost:3002/api-docs/'
    ) {
      return next();
    }

    if (!(token && session && timestamp && signature)) {
      return next(
        new AuthError('Token, session, timestamp and signature are required')
      );
    }

    const userInfo = await redis.get(token);
    if (!userInfo) {
      return next(new AuthError('Invalid token'));
    }
    const { user, sessions } = JSON.parse(userInfo);

    const userToken = encrypt(user.id);

    if (userToken !== token) {
      return next(new AuthError('Wrong token'));
    }

    if (!sessions.includes(session)) {
      return next(new AuthError('Invalid session id'));
    }

    const uri = url.parse(req.url).pathname.toLowerCase();
    const method = req.method.toLowerCase();
    const body = Object.keys(req.body).length ? JSON.stringify(req.body) : '';

    const signatureRawData = method + uri + body + session + timestamp;
    const signatureRaw = crypto
      .createHash('sha256')
      .update(signatureRawData)
      .digest('hex');
    const hmac = crypto
      .createHmac('sha256', secret)
      .update(signatureRaw)
      .digest('hex');

    if (hmac === signature) {
      req.user = user;
      req.userPermission = BigInt(user.permission || 0);
      req.departmentPermission = BigInt(
        user.department ? user.department.permission : 0
      );
      req.rolePermission = BigInt(user.role ? user.role.permission : 0);
      return next();
    }
    return next(new AuthError());
  } catch (error) {
    return next(error);
  }
}
