/* eslint-disable no-console */
import http from 'http';
import app from './app';

// const ngrok = require('ngrok');

process.env.NODE_ENV = (process.env.NODE_ENV || 'development');

const server = http.createServer(app());
server.listen(process.env.PORT, process.env.HOST, () => {
  console.log('api listening at http://%s:%s', server.address().address, server.address().port, ' pid: ', process.pid);
  console.log(new Date().toLocaleString());
});

// ngrok
//   .connect({
//     addr: 3000,
//   })
//   .then((url) => {
//     console.log(`💳  App URL to see the demo in your browser: ${url}/`);
//   })
//   .catch((err) => {
//     if (err.code === 'ECONNREFUSED') {
//       console.log(`⚠️  Connection refused at ${err.address}:${err.port}`);
//     } else {
//       console.log(`⚠️ Ngrok error: ${JSON.stringify(err)}`);
//     }
//     process.exit(1);
//   });
